package de.craften.plugins.skyblockjsonapi.entities;

import de.craften.plugins.skyblockjsonapi.util.jsonize.Jsonizable;

import java.util.HashMap;
import java.util.Map;

/**
 * A two-dimensional point with an x- and a z-coordinate.
 */
public class Point implements Jsonizable {
    private int x;
    private int z;

    public Point(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    @Override
    public int hashCode() {
        return x + 13 * z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point other = (Point) obj;
            return other.x == x && other.z == z;
        }
        return false;
    }

    @Override
    public Object jsonize() {
        Map<String, Object> result = new HashMap<>();
        result.put("x", x);
        result.put("z", z);
        return result;
    }
}
