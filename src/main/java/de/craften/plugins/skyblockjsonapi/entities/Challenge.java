package de.craften.plugins.skyblockjsonapi.entities;


import de.craften.plugins.skyblockjsonapi.util.jsonize.Jsonizable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * A challenge for a player.
 */
public class Challenge implements Jsonizable {
    private String name;
    private Date firstCompleted;
    private int timesCompleted;

    public Challenge(String name, long firstCompleted, int timesCompleted) {
        this.name = name;
        if (firstCompleted > 0)
            this.firstCompleted = new Date(firstCompleted);
        this.timesCompleted = timesCompleted;
    }

    /**
     * Gets the name of the challenge. This might not be a beautiful name to display.
     *
     * @return Name of this challenge
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the date when this challenge was first completed by the player.
     *
     * @return The date when this challenge was first completed by the player or null if it was never completed
     */
    public Date getFirstCompleted() {
        return firstCompleted;
    }

    /**
     * Gets the number of times the player completed this challenge.
     *
     * @return The number of times the player completed this challenge
     */
    public int getTimesCompleted() {
        return timesCompleted;
    }

    @Override
    public Object jsonize() {
        Map<String, Object> result = new HashMap<>();
        result.put("firstCompleted", firstCompleted != null ? firstCompleted.getTime() : false);
        result.put("timesCompleted", timesCompleted);
        return result;
    }
}
