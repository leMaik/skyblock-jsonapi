package de.craften.plugins.skyblockjsonapi.entities;


import de.craften.plugins.skyblockjsonapi.util.jsonize.Jsonize;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * A uSkyblock player. Instances of this class may also contain the island of the player and islands the player also builds on.
 *
 * @see de.craften.plugins.skyblockjsonapi.entities.IslandPlayer
 */
public class DeepIslandPlayer extends IslandPlayer {
    private Island island;
    private List<Island> otherIslands;

    public DeepIslandPlayer(File yamlFile) {
        super(yamlFile);
        otherIslands = new ArrayList<>();
    }

    /**
     * Gets the island of the player.
     *
     * @return The island of the player or null if it's not set
     */
    public Island getIsland() {
        return island;
    }

    /**
     * Sets the island of the player
     *
     * @param island This player's island
     */
    public void setIsland(Island island) {
        this.island = island;
    }

    /**
     * Gets foreign islands this player builds on. Does not include the player's own island.
     *
     * @return Foreign islands this player builds on
     */
    public List<Island> getOtherIslands() {
        return Collections.unmodifiableList(otherIslands);
    }

    /**
     * Adds an island this player builds on.
     *
     * @param island An island this player builds on
     */
    public void addOtherIsland(Island island) {
        otherIslands.add(island);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object jsonize() {
        Map<String, Object> result = (Map) super.jsonize();
        result.put("island", getIsland() != null ? getIsland().jsonize() : null);
        result.put("otherIslands", Jsonize.collection(otherIslands));
        return result;
    }
}
