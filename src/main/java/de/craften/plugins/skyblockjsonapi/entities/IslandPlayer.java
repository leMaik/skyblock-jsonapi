package de.craften.plugins.skyblockjsonapi.entities;


import de.craften.plugins.skyblockjsonapi.util.jsonize.Jsonizable;
import de.craften.plugins.skyblockjsonapi.util.jsonize.Jsonize;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.*;

/**
 * An uSkyblock player.
 */
public class IslandPlayer implements Jsonizable {
    private boolean hasIsland;
    private Point islandPosition;
    protected List<Challenge> challenges;

    protected IslandPlayer(boolean hasIsland, int posX, int posZ) {
        this.hasIsland = hasIsland;

        if (hasIsland) {
            this.islandPosition = new Point(posX, posZ);
        } else {
            this.islandPosition = null;
        }
    }

    public IslandPlayer(File yamlFile) {
        YamlConfiguration yaml = YamlConfiguration.loadConfiguration(yamlFile);
        hasIsland = yaml.getBoolean("player.hasIsland");
        islandPosition = new Point(yaml.getInt("player.islandX"), yaml.getInt("player.islandZ"));

        Set<String> challenges = yaml.getConfigurationSection("player.challenges").getKeys(false);
        this.challenges = new ArrayList<>(challenges.size());
        for (String challenge : challenges) {
            this.challenges.add(new Challenge(
                    challenge,
                    yaml.getLong("player.challenges." + challenge + ".firstCompleted"),
                    yaml.getInt("player.challenges." + challenge + ".timesCompleted")));
        }
    }

    /**
     * Gets the position of this player's island.
     * @return The position of this player's island
     */
    public Point getIslandPosition() {
        return islandPosition;
    }

    @Override
    public Object jsonize() {
        Map<String, Object> result = new HashMap<>();
        result.put("hasIsland", hasIsland);
        result.put("islandPosition", islandPosition.jsonize());
        result.put("challenges", Jsonize.map(challenges, new Jsonize.KeyRetreiver<Challenge>() {
            @Override
            public String getKey(Challenge value) {
                return value.getName();
            }
        }));

        return result;
    }
}
