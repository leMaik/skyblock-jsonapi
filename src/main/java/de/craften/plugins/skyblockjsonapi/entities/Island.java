package de.craften.plugins.skyblockjsonapi.entities;

import com.google.common.collect.Lists;
import de.craften.plugins.skyblockjsonapi.util.jsonize.Jsonizable;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

/**
 * An uSkyBlock island.
 */
public class Island implements Jsonizable {
    private static final Pattern fileNamePattern = Pattern.compile("(-?\\d+),(-?\\d+)\\.yml");

    private String leader;
    private int posX;
    private int posZ;
    private int level;
    private List<String> party;

    public Island(File file, int x, int z) {
        this.posX = x;
        this.posZ = z;

        ConfigurationSection c = YamlConfiguration.loadConfiguration(file);
        leader = c.getString("party.leader");
        level = c.getInt("general.level");

        ConfigurationSection partySection = c.getConfigurationSection("party.members");
        party = partySection != null ? Lists.newArrayList(partySection.getKeys(false)) : new ArrayList<String>(0);
    }

    /**
     * Gets the leader of this island.
     *
     * @return The leader of this island
     */
    public String getLeader() {
        return leader;
    }

    /**
     * Gets the x-coordinate of the center of this island.
     *
     * @return The x-coordinate of this island
     */
    public double getX() {
        return posX;
    }

    /**
     * Gets the z-coordinate of the center of this island.
     *
     * @return The z-coordinate of this island
     */
    public double getZ() {
        return posZ;
    }

    /**
     * Gets the level of this island.
     *
     * @return The level of this island
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets the party of this island.
     *
     * @return The party of this island
     */
    public Collection<String> getParty() {
        return party;
    }

    @Override
    public Object jsonize() {
        Map<String, Object> result = new HashMap<>();
        result.put("x", getX());
        result.put("z", getZ());
        result.put("leader", getLeader());
        result.put("level", getLevel());
        result.put("party", getParty());
        return result;
    }
}
