package de.craften.plugins.skyblockjsonapi.util.jsonize;


public interface Jsonizable {
    public Object jsonize();
}
