package de.craften.plugins.skyblockjsonapi;

import com.alecgorge.minecraft.jsonapi.JSONAPI;
import com.alecgorge.minecraft.jsonapi.api.APIMethodName;
import com.alecgorge.minecraft.jsonapi.api.JSONAPICallHandler;
import de.craften.plugins.skyblockjsonapi.entities.DeepIslandPlayer;
import de.craften.plugins.skyblockjsonapi.entities.Island;
import de.craften.plugins.skyblockjsonapi.entities.IslandPlayer;
import de.craften.plugins.skyblockjsonapi.util.jsonize.Jsonize;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A plugin that adds JSONAPI methods for uSkyblock.
 */
public class SkyblockJsonapiPlugin extends JavaPlugin implements JSONAPICallHandler {
    private static final Pattern islandFileNamePattern = Pattern.compile("(-?\\d+),(-?\\d+)\\.yml", Pattern.CASE_INSENSITIVE);
    private static final Pattern playerFileNamePattern = Pattern.compile("(.+?)\\.yml", Pattern.CASE_INSENSITIVE);

    private File islandsDirectory;
    private File playersDirectory;

    private List<Island> cachedRankings = null;
    private Map<String, DeepIslandPlayer> cachedPlayers = null;

    @Override
    public void onEnable() {
        islandsDirectory = new File(new File(getDataFolder().getParentFile(), "uSkyBlock"), "islands");
        playersDirectory = new File(new File(getDataFolder().getParentFile(), "uSkyBlock"), "players");

        JSONAPI jsonapi = (JSONAPI) getServer().getPluginManager().getPlugin("JSONAPI");
        jsonapi.registerAPICallHandler(this);

        getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                cachedPlayers = getDeepPlayersNetwork();
            }
        }, 0, 20 * 60 * 60); //1h

        getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                cachedRankings = getRanking();
            }
        }, 0, 20 * 60 * 60); //1h
    }

    @Override
    public boolean willHandle(APIMethodName apiMethodName) {
        if (!apiMethodName.getNamespace().equals("skyblock"))
            return false;

        if (apiMethodName.getMethodName().equals("player"))
            return true;
        if (apiMethodName.getMethodName().equals("playerDeep"))
            return true;
        if (apiMethodName.getMethodName().equals("island"))
            return true;
        if (apiMethodName.getMethodName().equals("islandRanking"))
            return true;

        return false;
    }

    @Override
    public Object handle(APIMethodName apiMethodName, Object[] objects) {
        if (apiMethodName.getMethodName().equals("player") && objects.length == 1) {
            File player = new File(playersDirectory, objects[0].toString() + ".yml");
            if (player.exists() && player.canRead()) {
                return new IslandPlayer(player).jsonize();
            } else {
                return false;
            }
        } else if (apiMethodName.getMethodName().equals("playerDeep") && objects.length == 1) {
            if (cachedPlayers == null) {
                getLogger().warning("Deep players network requested but not yet available");
                return false;
            }
            try {
                DeepIslandPlayer player = cachedPlayers.get(objects[0].toString());
                if (player != null)
                    return player.jsonize();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (apiMethodName.getMethodName().equals("island") && objects.length == 2) {
            try {
                int x = Integer.parseInt(objects[0].toString());
                int z = Integer.parseInt(objects[1].toString());

                File island = new File(islandsDirectory, x + "," + z + ".yml");
                if (island.exists() && island.canRead()) {
                    return new Island(island, x, z).jsonize();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (apiMethodName.getMethodName().equals("islandRanking")) {
            if (cachedRankings == null) {
                getLogger().warning("Rankings requested but not yet available");
                return false;
            }
            return Jsonize.collection(cachedRankings);
        }

        return null;
    }

    private List<Island> getRanking() {
        List<Island> ranking = new ArrayList<>();
        for (File island : islandsDirectory.listFiles(new YamlFileFilter())) {
            Matcher m = islandFileNamePattern.matcher(island.getName());
            if (m.matches()) {
                ranking.add(new RankedIsland(island, Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))));
            }
        }

        Collections.sort(ranking, new IslandLevelComparator());
        int last = Integer.MAX_VALUE;
        int place = 0;
        int cnt = 1;
        for (Island island : ranking) {
            if (island.getLevel() < last) {
                place += cnt;
                cnt = 1;
            } else {
                cnt++;
            }
            last = island.getLevel();
            ((RankedIsland) island).setRank(place);
        }
        return ranking;
    }

    private Map<String, DeepIslandPlayer> getDeepPlayersNetwork() {
        Map<String, DeepIslandPlayer> players = new HashMap<>();
        for (File player : playersDirectory.listFiles(new YamlFileFilter())) {
            Matcher m = playerFileNamePattern.matcher(player.getName());
            if (m.matches()) {
                players.put(m.group(1), new DeepIslandPlayer(player));
            }
        }

        for (File island : islandsDirectory.listFiles(new YamlFileFilter())) {
            Matcher m = islandFileNamePattern.matcher(island.getName());
            if (m.matches()) {
                Island is = new Island(island, Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
                DeepIslandPlayer leader = players.get(is.getLeader());
                if (leader != null) {
                    leader.setIsland(is);
                }

                for (String member : is.getParty()) {
                    DeepIslandPlayer mem = players.get(member);
                    if (mem != null) {
                        if (!member.equals(is.getLeader())) {
                            mem.addOtherIsland(is);
                        }
                    }
                }
            }
        }

        return players;
    }

    private static class YamlFileFilter implements FilenameFilter {
        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(".yml");
        }
    }

    private static class IslandLevelComparator implements Comparator<Island> {
        @Override
        public int compare(Island island, Island island2) {
            return island2.getLevel() - island.getLevel();
        }
    }

    private static class RankedIsland extends Island {
        private int rank = 0;

        public RankedIsland(File file, int x, int z) {
            super(file, x, z);
        }

        public int getRank() {
            return rank;
        }

        public void setRank(int rank) {
            this.rank = rank;
        }

        @SuppressWarnings("unchecked")
        @Override
        public Object jsonize() {
            Map<String, Object> result = (Map) super.jsonize();
            result.put("rank", rank);
            return result;
        }
    }
}
