# skyblock-jsonapi #

This badly-named plugin adds some methods for [JSONAPI](http://mcjsonapi.com/) to access [uSkyBlock](http://dev.bukkit.org/bukkit-plugins/ultimate-skyblock/) islands and players. It also supports a (cached) ranking by island levels.

## Requirements and Installation ##

This plugin depends on JSONAPI. It oviously also requires uSkyBlock (2.0) to work. Install all these plugins and that's it. No configuration needed.

## JSONAPI methods ##

While this plugins doesn't add any commands at all, it adds the following new JSONAPI methods:

| Name                   | Parameters           | Description                                                                     |
|------------------------|----------------------|---------------------------------------------------------------------------------|
| skyblock.player        | playername           | Gets all information about the given player.                                    |
| skyblock.playerDeep    | playername           | Gets all information about the given player, including the island.              |
| skyblock.island        | positionX, positionZ | Gets the island at the given location. (Must be exactly the island's position!) |
| skyblock.islandRanking | *none*               | Gets a ranking of all islands                                                   |